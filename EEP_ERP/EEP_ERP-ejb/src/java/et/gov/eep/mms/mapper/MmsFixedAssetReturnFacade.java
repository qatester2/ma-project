
package et.gov.eep.mms.mapper;

import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.mms.entity.MmsFixedAssetReturn;
import et.gov.eep.mms.entity.MmsFixedassetRegstDetail;
import et.gov.eep.mms.entity.MmsFixedassetRegstration;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.eclipse.persistence.internal.queries.ArrayListContainerPolicy;

/**
 *
 * @author w_station
 */
@Stateless
public class MmsFixedAssetReturnFacade extends AbstractFacade<MmsFixedAssetReturn> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MmsFixedAssetReturnFacade() {
        super(MmsFixedAssetReturn.class);
    }

    public ArrayList<MmsFixedAssetReturn> searchByReturnNo(MmsFixedAssetReturn returnEntity) {
        Query query = em.createNamedQuery("MmsFixedAssetReturn.findByFarNoLike");
        query.setParameter("farNo", returnEntity.getFarNo() + '%');
        System.out.println("======facade=====" + returnEntity.getFixedAssetList());
        try {
            ArrayList<MmsFixedAssetReturn> listofFARNo = new ArrayList(query.getResultList());
            return listofFARNo;
        } catch (Exception ex) {
            return null;
        }

    }

    public MmsFixedAssetReturn getLastReturnId() {

        Query query1 = em.createNamedQuery("MmsFixedAssetReturn.findByFarIdMaximum");

        MmsFixedAssetReturn result = null;

        try {
            if (query1.getResultList().size() > 0) {
                result = (MmsFixedAssetReturn) query1.getResultList().get(0);
            } else {
                return result;
            }

            return result;
        } catch (Exception ex) {

            ex.printStackTrace();
            return null;
        }
    }

    public List<MmsFixedAssetReturn> searchReturnByParameterPrefix(MmsFixedAssetReturn returnEntity) {
        Query query = em.createNamedQuery("MmsFixedAssetReturn.findByAllParameters");
        query.setParameter("farNo", '%' + returnEntity.getFarNo() + '%');
        try {
            ArrayList<MmsFixedAssetReturn> insuranceList = new ArrayList(query.getResultList());
            return insuranceList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<MmsFixedAssetReturn> searchReturnByParameterPrefixAndProcessedBy(MmsFixedAssetReturn returnEntity) {
        Query query = em.createNamedQuery("MmsFixedAssetReturn.findByAllParametersAndProcessedBy");
        query.setParameter("farNo", '%' + returnEntity.getFarNo() + '%');
        query.setParameter("processedBy", returnEntity.getProcessedBy());
        try {
            ArrayList<MmsFixedAssetReturn> insuranceList = new ArrayList(query.getResultList());
            return insuranceList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<MmsFixedAssetReturn> searchByDept(MmsFixedAssetReturn returnEntity) {
        Query query = em.createNamedQuery("MmsFixedAssetReturn.findByAllParameters2");
        query.setParameter("department", '%' + returnEntity.getFarNo() + '%');
        try {
            ArrayList<MmsFixedAssetReturn> insuranceList = new ArrayList(query.getResultList());
            return insuranceList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public MmsFixedAssetReturn getSelectedRequest(Integer farId) {
        Query query = em.createNamedQuery("MmsFixedAssetReturn.findByFarId");
        query.setParameter("farId", farId);
        System.err.println("===" + query.getResultList().size());
        try {
            MmsFixedAssetReturn selectrequest = (MmsFixedAssetReturn) query.getResultList().get(0);
            return selectrequest;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<MmsFixedAssetReturn> findFarListByWfStatus(int status) {
        Query query = em.createNamedQuery("MmsFixedAssetReturn.findFarListByWfStatus", MmsFixedAssetReturn.class);
        query.setParameter("status", status);
        try {
            ArrayList<MmsFixedAssetReturn> listofsr = new ArrayList(query.getResultList());
            return listofsr;
        } catch (Exception e) {
            return null;
        }
    }

    public List<MmsFixedAssetReturn> findFarListForCheckerByWfStatus(int preparerStatus, int approverRejectStatus) {
        Query query = em.createNamedQuery("MmsFixedAssetReturn.findFarListForCheckerByWfStatus", MmsFixedAssetReturn.class);
        query.setParameter("prepared", preparerStatus);
        query.setParameter("approverReject", approverRejectStatus);

        try {
            ArrayList<MmsFixedAssetReturn> listofsr = new ArrayList(query.getResultList());
            return listofsr;
        } catch (Exception e) {
            return null;
        }
    }

    public List<MmsFixedAssetReturn> searchAllFarInfoByPreparerId(Integer processedBy) {
        Query query = em.createNamedQuery("MmsFixedAssetReturn.findAllByPreparerId", MmsFixedAssetReturn.class);
        query.setParameter("processedBy", processedBy);
        System.out.println("======@facade====" + processedBy);
        try {
            ArrayList<MmsFixedAssetReturn> LocList = new ArrayList(query.getResultList());
            return LocList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<MmsFixedassetRegstDetail> getReturnByDepId(MmsFixedassetRegstration fixedassetRegstration) {
        System.out.println("here");
        Query q = em.createNamedQuery("select dt.* from mms_fixedasset_regst_detail dt\n"
                + "inner join mms_fixedasset_regstration ma\n"
                + "on dt.far_id=ma.id\n"
                + "where ma.id='" + fixedassetRegstration.getId() + "'");
        try {
            System.out.println("===");
            List<MmsFixedassetRegstDetail> returnBys = new ArrayList<>();
            if (q.getResultList().size() > 0) {
                System.out.println("==========" + returnBys.size());
                returnBys = q.getResultList();
            }
            return returnBys;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Integer> getEmpIdByName(String returnby) {
        System.out.println("facade " + returnby);
        Query q = em.createNativeQuery("select hr.id from hr_employees hr\n"
                + "where hr.first_name='" + returnby + "'");
        try {
            List<Integer> EmpId = new ArrayList();
            if (q.getResultList().size() > 0) {
                EmpId = q.getResultList();
                System.out.println("size     " + EmpId.size());
            }
            return EmpId;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public MmsFixedassetRegstDetail gettingTagInfo(String tingOthersByTagNo) {
        System.out.println("get here " + tingOthersByTagNo);
        Query query = em.createNativeQuery("SELECT * from mms_fixedasset_regst_detail \n"
                + "where tag_no='" + tingOthersByTagNo + "'", MmsFixedassetRegstDetail.class);
        try {
            MmsFixedassetRegstDetail tagInfo = new MmsFixedassetRegstDetail();
            if (query.getResultList().size() > 0) {
                System.out.println(" existed");
                tagInfo = (MmsFixedassetRegstDetail) query.getResultList().get(0);
                System.out.println(" " + tagInfo.getItemName());
                System.out.println(" " + tagInfo.getItemId().getMatCode());
            }
            return tagInfo;
        } catch (Exception e) {
            return null;
        }
    }

}
