/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package et.gov.eep.hrms.mapper.recruitment;

import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.hrms.entity.lookup.HrLuEducLevels;
import et.gov.eep.hrms.entity.lookup.HrLuEducTypes;
import et.gov.eep.hrms.entity.lookup.HrLuLanguages;
import et.gov.eep.hrms.entity.lookup.HrLuNationalities;
import et.gov.eep.hrms.entity.recruitment.HrAdvertisedJobs;
import et.gov.eep.hrms.entity.recruitment.HrAdvertisements;
import et.gov.eep.hrms.entity.recruitment.HrCandidiates;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author user
 */
@Stateless
public class HrCandidiatesFacade extends AbstractFacade<HrCandidiates> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HrCandidiatesFacade() {
        super(HrCandidiates.class);
    }

    public List<HrAdvertisements> batchCodes(String type) {
        Query query = em.createNativeQuery("SELECT adv.* "
                + " FROM HR_ADVERTISEMENTS adv "
                + " WHERE adv.ADVERT_TYPE = '" + type + "'"
                + " order by adv.ID DESC", HrAdvertisements.class);
        try {
            return (List<HrAdvertisements>) query.getResultList();
        } catch (Exception ex) {
            return null;
        }
    }

//    AND h.endDate >= CURRENT_DATE
    public List<HrAdvertisedJobs> advertizedJobs(int advertId) {
        Query query = em.createNativeQuery("SELECT HR_ADVERTISED_JOBS.*,"
                + " HR_JOB_TYPES.JOB_TITLE "
                + " FROM  HR_ADVERTISED_JOBS, "
                + "       HR_JOB_TYPES "
                + " WHERE HR_ADVERTISED_JOBS.JOB_ID=HR_JOB_TYPES.ID "
                + "       AND HR_ADVERTISED_JOBS.ADVERT_ID='" + advertId + "'",
                HrAdvertisedJobs.class);
        try {
            return (List<HrAdvertisedJobs>) query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public HrAdvertisements findBatchCode(HrAdvertisements hrAdvertisements) {
        Query query = em.createNamedQuery("HrAdvertisements.findByBatchCode");
        query.setParameter("batchCode", hrAdvertisements.getBatchCode());
        try {
            HrAdvertisements advertisements = (HrAdvertisements) query.getResultList().get(0);
            return advertisements;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public HrLuNationalities findNationality(HrLuNationalities hrLuNationalities) {
        Query query = em.createNamedQuery("HrLuNationalities.findByNationality");
        query.setParameter("nationality", hrLuNationalities.getNationality());
        try {
            HrLuNationalities Nationalityname = (HrLuNationalities) query.getResultList().get(0);
            return Nationalityname;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public ArrayList<HrLuEducTypes> findEducationTypes() {
        Query query = em.createNamedQuery("HrLuEducTypes.findAll");
        try {
            ArrayList<HrLuEducTypes> types = new ArrayList(query.getResultList());
            return types;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public ArrayList<HrLuEducLevels> findEducationLeves() {
        Query query = em.createNamedQuery("HrLuEducLevels.findAll");
        try {
            ArrayList<HrLuEducLevels> levels = new ArrayList(query.getResultList());
            return levels;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public HrCandidiates getByCandidateId(HrCandidiates candidiates) {
        Query query = em.createNamedQuery("HrCandidiates.findById", HrCandidiates.class);
        query.setParameter("id", candidiates.getId());
        try {
            HrCandidiates Candidate = (HrCandidiates) query.getResultList().get(0);
            return Candidate;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public ArrayList<HrCandidiates> searchByCandidateName(HrCandidiates candidiates) {
        Query query = em.createNamedQuery("HrCandidiates.findByFirstNameLike", HrCandidiates.class);
        query.setParameter("firstName", candidiates.getFirstName().toUpperCase() + '%');
        try {
            ArrayList<HrCandidiates> Candidate = new ArrayList(query.getResultList());
            return Candidate;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public HrCandidiates getByfirstName(HrCandidiates candidiates) {
        Query query = em.createNamedQuery("HrCandidiates.findByFirstName", HrCandidiates.class);
        query.setParameter("firstName", candidiates.getFirstName());
        try {
            HrCandidiates candi = (HrCandidiates) (query.getResultList().get(0));
            return candi;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public ArrayList<HrLuLanguages> findAllHrLuLanguages() {
        Query query = em.createNamedQuery("HrLuLanguages.findAll");
        try {
            ArrayList<HrLuLanguages> list = new ArrayList(query.getResultList());
            return list;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    //<editor-fold defaultstate="collapsed" desc="recruitment for employee">
    public List<HrCandidiates> readCandidiates(int status1, int status2) {
        Query query = em.createNativeQuery("SELECT HR_CANDIDIATES.* "
                //                + "FIRST_NAME || ' ' | MIDDLE_NAME || ' ' || LAST_NAME AS FULLNAME "
                + "FROM HR_CANDIDIATES "
                + "WHERE STATUS=? OR STATUS=? ", HrCandidiates.class);
        query.setParameter(1, status1);
        query.setParameter(2, status2);
        try {
            return (List<HrCandidiates>) query.getResultList();
        } catch (Exception ex) {
            return null;
        }
    }
    //</editor-fold>
}
