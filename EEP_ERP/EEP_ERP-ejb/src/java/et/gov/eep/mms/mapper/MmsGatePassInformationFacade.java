/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package et.gov.eep.mms.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.mms.entity.MmsGatePassInformation;

/**
 *
 * @author Minab
 */
@Stateless
public class MmsGatePassInformationFacade extends AbstractFacade<MmsGatePassInformation> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     *
     */
    public MmsGatePassInformationFacade() {
        super(MmsGatePassInformation.class);
    }

    /**
     *
     * @param gatePassEntity
     * @return
     */
    //<editor-fold defaultstate="collapsed" desc="NamedQuery">
    public ArrayList<MmsGatePassInformation> searchGatePassByParameterPrefix(MmsGatePassInformation gatePassEntity) {
        Query query = em.createNamedQuery("MmsGatePassInformation.findByAllParameters");
        query.setParameter("gatePassNo", gatePassEntity.getGatePassNo() + '%');
        try {
            ArrayList<MmsGatePassInformation> gatePassList = new ArrayList(query.getResultList());
            return gatePassList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public ArrayList<MmsGatePassInformation> searchGatePassByParameterPrefixAndProcessedBy(MmsGatePassInformation gatePassEntity) {
        Query query = em.createNamedQuery("MmsGatePassInformation.findByAllParametersAndProcessedBy");
        query.setParameter("gatePassNo", gatePassEntity.getGatePassNo() + '%');
        query.setParameter("processedBy", gatePassEntity.getProcessedBy());
        try {
            ArrayList<MmsGatePassInformation> gatePassList = new ArrayList(query.getResultList());
            return gatePassList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public ArrayList<MmsGatePassInformation> searchGatePassByParameterContains(MmsGatePassInformation gatePassEntity) {
        Query query = em.createNamedQuery("MmsGatePassInformation.findByAllParameters");
        query.setParameter("gatePassNo", '%' + gatePassEntity.getGatePassNo() + '%');
        try {
            ArrayList<MmsGatePassInformation> gatePassList = new ArrayList(query.getResultList());
            return gatePassList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public ArrayList<MmsGatePassInformation> searchGatePassByProcessedBy(MmsGatePassInformation gatePassEntity) {
        Query query = em.createNamedQuery("MmsGatePassInformation.findByProcessedBy");
        query.setParameter("Processedby", '%' + gatePassEntity.getProcessedBy() + '%');
        try {
            ArrayList<MmsGatePassInformation> gatePassList = new ArrayList(query.getResultList());
            return gatePassList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @return
     */
    public MmsGatePassInformation getLastGetpassNo() {
        Query query = em.createNamedQuery("MmsGatePassInformation.findByGatepassIdMaximum");
        MmsGatePassInformation result = null;
        try {
            if (query.getResultList().size() > 0) {
                result = (MmsGatePassInformation) query.getResultList().get(0);
            }

            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
 //</editor-fold>
}
