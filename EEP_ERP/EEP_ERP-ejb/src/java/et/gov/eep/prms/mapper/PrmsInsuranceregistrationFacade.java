/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package et.gov.eep.prms.mapper;

import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.fcms.entity.FmsLuCurrency;
import et.gov.eep.prms.entity.PrmsServiceProvider;
import et.gov.eep.prms.entity.PrmsInsuranceRequisition;
import et.gov.eep.prms.entity.PrmsServiceProviderDetail;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author user
 */
@Stateless
public class PrmsInsuranceregistrationFacade extends AbstractFacade<PrmsInsuranceRequisition> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PrmsInsuranceregistrationFacade() {
        super(PrmsInsuranceRequisition.class);
    }

    // <editor-fold defaultstate="collapsed" desc="Named(Static) Queries">
    public List<PrmsInsuranceRequisition> searchByInsuranceNo(PrmsInsuranceRequisition insuranceregistration) {
        Query query = em.createNamedQuery("PrmsInsuranceRequisition.searchByInsuranceNo");
        query.setParameter("insuranceNo", insuranceregistration.getInsuranceNo() + "%");
        query.setParameter("preparedBy", insuranceregistration.getPreparedBy());
        try {
            ArrayList<PrmsInsuranceRequisition> InsuranceList = new ArrayList<>(query.getResultList());
            return InsuranceList;

        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }
    }

    public PrmsInsuranceRequisition getSelectedRow(String insuranceNo) {
        Query query = em.createNamedQuery("PrmsInsuranceRequisition.findForRowSelect");
        query.setParameter("insuranceNo", insuranceNo);
        try {
            PrmsInsuranceRequisition insuranceList = (PrmsInsuranceRequisition) query.getResultList().get(0);
            return insuranceList;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List<PrmsInsuranceRequisition> generateNextInsuranceNo(String prefix, String eYear) {
        Query query = em.createNamedQuery("PrmsInsuranceRequisition.findByInsuranceNos");
        query.setParameter("insuranceNo", prefix + "-" + '%' + "/" + eYear);
        List<PrmsInsuranceRequisition> insuNoList = new ArrayList<>();
        try {
            if (query.getResultList().size() > 0) {
                insuNoList = query.getResultList();
            }
            return insuNoList;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public List<PrmsInsuranceRequisition> getInsuRequestLists() {
        Query query = em.createNamedQuery("PrmsInsuranceRequisition.findByInsuranceForApproval", PrmsInsuranceRequisition.class);
        ArrayList<PrmsInsuranceRequisition> insuranceRequestList = new ArrayList(query.getResultList());
        return insuranceRequestList;
    }
    // </editor-fold>

}
