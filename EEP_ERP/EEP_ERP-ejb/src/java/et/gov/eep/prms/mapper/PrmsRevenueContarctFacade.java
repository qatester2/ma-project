/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package et.gov.eep.prms.mapper;

import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.fcms.entity.FmsLuCurrency;
import et.gov.eep.prms.entity.PrmsRevenueContarct;
import et.gov.eep.prms.entity.PrmsRevenueContractDetail;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author mora1
 */
@Stateless
public class PrmsRevenueContarctFacade extends AbstractFacade<PrmsRevenueContarct> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PrmsRevenueContarctFacade() {
        super(PrmsRevenueContarct.class);
    }

    public List<FmsLuCurrency> currencyNameLists() {
        Query query = em.createNamedQuery("FmsLuCurrency.findAll");
        try {
            List<FmsLuCurrency> currLists = new ArrayList<>();
            if (query.getResultList().size() > 0) {
                currLists = query.getResultList();
            }
            return currLists;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<PrmsRevenueContarct> searchAllRevContract() {
        Query query = em.createNamedQuery("PrmsRevenueContarct.findAll");
        try {
            List<PrmsRevenueContarct> allRevContList = new ArrayList<>();
            if (query.getResultList().size() > 0) {
                allRevContList = query.getResultList();
                System.out.println("hhh " + allRevContList.size());
            }
            return allRevContList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<PrmsRevenueContarct> getRevContSeqNo(String prefix, String ethYear) {
        Query query = em.createNamedQuery("PrmsRevenueContarct.findByRevNoLike");
        query.setParameter("revContractNo", prefix + "-" + "%" + "/" + ethYear);
        List<PrmsRevenueContarct> seqNoLists = new ArrayList<>();
        if (query.getResultList().size() > 0) {
            seqNoLists = query.getResultList();
        }
        return seqNoLists;
    }

}
