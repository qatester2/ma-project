package et.gov.eep.fcms.mapper.bank;

import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.fcms.entity.bank.FmsBank;

/**
 *
 * @author mubejbl
 */
@Stateless
public class FmsBankFacade extends AbstractFacade<FmsBank> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FmsBankFacade() {
        super(FmsBank.class);
    }

      /*named query to select bank list from FmsBank table using bankName's first letter match
    returen bank List*/
    public ArrayList<FmsBank> searchBankByName(FmsBank bankName) {
        Query query = em.createNamedQuery("FmsBank.findByBankNameLike");
        query.setParameter("bankName", bankName.getBankName().toUpperCase() + '%');
        try {
            ArrayList<FmsBank> bankList = new ArrayList(query.getResultList());
            return bankList;

        } catch (Exception ex) {

            return null;
        }

    }

      /*named query to select bank information from FmsBank table using bankName
    returen bank information*/
    public FmsBank getBankInfo(FmsBank bankName) {
        Query query = em.createNamedQuery("FmsBank.findByBankName");
        query.setParameter("bankName", bankName.getBankName());
        try {
            FmsBank bankInfo = (FmsBank) query.getResultList().get(0);
            return bankInfo;
        } catch (Exception ex) {

            return null;
        }
    }

       /*named query to select bank information from FmsBank table using bankName
    returen boolean value*/
    public boolean findDupByBankName(FmsBank fmsBank) {
        boolean dup;
        try {
            Query query = em.createNamedQuery("FmsBank.findByBankName");
            query.setParameter("bankName", fmsBank.getBankName());
            if (query.getResultList().size() > 0) {
                dup = true;
            } else {
                dup = false;
            }
            return dup;
        } catch (Exception ex) {
            return false;
        }
    }

     /*named query to select bank information from FmsBank table using bankCode
    returen boolean value*/
    public boolean findDupByBankCode(FmsBank fmsBank) {
        boolean dup;
        try {
            Query query = em.createNamedQuery("FmsBank.findByBankCode");
            query.setParameter("bankCode", fmsBank.getBankCode());
            if (query.getResultList().size() > 0) {
                dup = true;
            } else {
                dup = false;
            }
            return dup;
        } catch (Exception ex) {
            return false;
        }
    }
}
