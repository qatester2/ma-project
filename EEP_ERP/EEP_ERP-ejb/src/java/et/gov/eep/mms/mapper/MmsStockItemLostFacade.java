
package et.gov.eep.mms.mapper;

import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.mms.entity.MmsStockItemLost;
import et.gov.eep.mms.entity.MmsStoreInformation;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author w_station
 */
@Stateless
public class MmsStockItemLostFacade extends AbstractFacade<MmsStockItemLost> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MmsStockItemLostFacade() {
        super(MmsStockItemLost.class);
    }

    public List<MmsStockItemLost> searchLostItemByParameterPrefix(MmsStockItemLost stockLostEntity) {
        Query query = em.createNamedQuery("MmsStockItemLost.findByAllParameters");
        query.setParameter("lostItemNo", '%' + stockLostEntity.getLostItemNo() + '%');

        try {
            ArrayList<MmsStockItemLost> lostItemList = new ArrayList(query.getResultList());
            return lostItemList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public MmsStockItemLost getSelectedRequest(BigDecimal lostStockId) {
        Query query = em.createNamedQuery("MmsStockItemLost.findByLostItemId");
        query.setParameter("lostStockId", lostStockId);
        System.err.println("===" + query.getResultList().size());
        try {
            MmsStockItemLost selectrequest = (MmsStockItemLost) query.getResultList().get(0);
            return selectrequest;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public MmsStockItemLost getLastLostItemId() {
        Query query = em.createNamedQuery("MmsStockItemLost.findBylostItemIdMaximum");
        MmsStockItemLost result = null;
        try {
            if (query.getResultList().size() > 0) {
                result = (MmsStockItemLost) query.getResultList().get(0);
            }

            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<MmsStockItemLost> searchLostStockByLostNoAndStoreId(MmsStockItemLost stockLostEntity, MmsStoreInformation storeInfoEntity) {
        Query query = em.createNamedQuery("MmsStockItemLost.findByStockNoAndStoreId", MmsStockItemLost.class);
        query.setParameter("lostItemNo", stockLostEntity.getLostItemNo() + '%');
        query.setParameter("storeId", storeInfoEntity);
        try {
            ArrayList<MmsStockItemLost> listofstock = new ArrayList(query.getResultList());
            return listofstock;
        } catch (Exception e) {
            return null;
        }
    }

    public List<MmsStockItemLost> searchLostItemByParameterPrefixAndLostPrep(MmsStockItemLost stockLostEntity) {
        System.out.println("------inside--");
        Query query = em.createNamedQuery("MmsStockItemLost.findByAllParametersAndLotPrep");
        query.setParameter("lostItemNo", stockLostEntity.getLostItemNo());
        query.setParameter("processedBy", stockLostEntity.getProcessedBy());
        System.out.println("========list===="+stockLostEntity);
        try {
            ArrayList<MmsStockItemLost> lostItemList = new ArrayList(query.getResultList());
            System.out.println("--------"+lostItemList);
            return lostItemList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<MmsStockItemLost> findSrListByWfStatus(int CHECK_APPROVE_VALUE) {
        Query query = em.createNamedQuery("MmsStockItemLost.findStkListByWfStatus", MmsStockItemLost.class);
        query.setParameter("stockStatus", CHECK_APPROVE_VALUE);
        try {
            ArrayList<MmsStockItemLost> listofstk = new ArrayList(query.getResultList());
            return listofstk;
        } catch (Exception e) {
            return null;
        }
    }

    public List<MmsStockItemLost> findSrListForCheckerByWfStatus(int PREPARE_VALUE, int APPROVE_REJECT_VALUE) {
        Query query = em.createNamedQuery("MmsStockItemLost.findStkListForCheckerByWfStatus", MmsStockItemLost.class);
        query.setParameter("prepared", PREPARE_VALUE);
        query.setParameter("approverReject", APPROVE_REJECT_VALUE);

        try {
            ArrayList<MmsStockItemLost> listofstk = new ArrayList(query.getResultList());
            return listofstk;
        } catch (Exception e) {
            return null;
        }
    }

    public List<MmsStockItemLost> searchAllLostInfoByPreparerId(Integer processedBy) {
        System.out.println("=========++++");
        Query query = em.createNamedQuery("MmsStockItemLost.findAllByPreparerId", MmsStockItemLost.class);

        query.setParameter("processedBy", processedBy);
        System.out.println("======@facade====" + processedBy);
        try {
            ArrayList<MmsStockItemLost> LocList = new ArrayList(query.getResultList());
            return LocList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
