/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package et.gov.eep.mms.mapper;

import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.hrms.entity.organization.HrDepartments;
import et.gov.eep.mms.entity.MmsLuWareHouse;
import et.gov.eep.mms.entity.MmsStoreToHrDepMapper;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Sadik
 */
@Stateless
public class MmsStoreToHrDepMapperFacade extends AbstractFacade<MmsStoreToHrDepMapper> {
    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MmsStoreToHrDepMapperFacade() {
        super(MmsStoreToHrDepMapper.class);
    }

    public boolean checkForDuplications(MmsStoreToHrDepMapper storeToDepartmentMapper) {
        boolean duplicaton;
        Query query = em.createNamedQuery("MmsStoreToHrDepMapper.findByDepartmentIdAndWarehouseId", MmsStoreToHrDepMapper.class);
        query.setParameter("warehouseId", storeToDepartmentMapper.getWarehouseId());
        query.setParameter("depId", storeToDepartmentMapper.getDepartmentId());
        try {
            if (query.getResultList().size() > 0) {
                duplicaton = true;
            } else {
                duplicaton = false;
            }
            return duplicaton;
        } catch (Exception ex) {
            return false;
        }
    
    }
    
    public List<MmsStoreToHrDepMapper> searchByWarehouseName(MmsLuWareHouse wareHouse){
        try{
        Query query1 = em.createNativeQuery("SELECT sm.*  "
                + "FROM mms_store_to_hr_dep_mapper sm          "
                + "INNER JOIN mms_lu_ware_house wh "
                + "ON sm.warehouse_id= wh.id "
                + "WHERE wh.name Like'" + wareHouse.getName() + "%' " ,
               
                MmsStoreToHrDepMapper.class);
          return (List<MmsStoreToHrDepMapper>) query1.getResultList();
        }
         catch (Exception ex) {
            return null;
        }
    }
    
    public List<MmsStoreToHrDepMapper> searchByDepartmentName(HrDepartments departments){
        try{
        Query query1 = em.createNativeQuery("SELECT sm.*  "
                + "FROM mms_store_to_hr_dep_mapper sm          "
                + "INNER JOIN hr_departments dep "
                + "ON sm.department_id= dep.dep_id "
                + "WHERE dep.dep_name Like'" + departments.getDepName() + "%' " ,
               
                MmsStoreToHrDepMapper.class);
          return (List<MmsStoreToHrDepMapper>) query1.getResultList();
        }
         catch (Exception ex) {
            return null;
        }
    }
  
    
}
