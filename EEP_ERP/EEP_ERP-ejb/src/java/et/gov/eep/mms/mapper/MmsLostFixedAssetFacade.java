
package et.gov.eep.mms.mapper;

import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.mms.entity.MmsLostFixedAsset;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author w_station
 */
@Stateless
public class MmsLostFixedAssetFacade extends AbstractFacade<MmsLostFixedAsset> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     *
     */
    public MmsLostFixedAssetFacade() {
        super(MmsLostFixedAsset.class);
    }

    /**
     *
     * @return
     */
    public MmsLostFixedAsset getLastLostItemId() {
        Query query = em.createNamedQuery("MmsLostFixedAsset.findBylostItemIdMaximum");
        MmsLostFixedAsset result = null;
        try {
            if (query.getResultList().size() > 0) {
                result = (MmsLostFixedAsset) query.getResultList().get(0);
            }

            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param lostitemno
     * @return
     */
    public ArrayList<MmsLostFixedAsset> searchByLostItemNo(MmsLostFixedAsset lostitemno) {
        Query query = em.createNamedQuery("MmsLostFixedAsset.findByLostItemNoLike");
        query.setParameter("lostItemNo", lostitemno.getLostItemNo() + '%');
        try {
            ArrayList<MmsLostFixedAsset> listoflostItemNo = new ArrayList(query.getResultList());
            return listoflostItemNo;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     *
     * @param lostFixedAsset
     * @return
     */
    public MmsLostFixedAsset getLostItemInfoByItemNo(MmsLostFixedAsset lostFixedAsset) {
        Query query = em.createNamedQuery("MmsLostFixedAsset.findByLostItemNo");
        query.setParameter("lostItemNo", lostFixedAsset.getLostItemNo());
        MmsLostFixedAsset result = null;
        try {
            if (query.getResultList().size() > 0) {
                result = (MmsLostFixedAsset) query.getResultList().get(0);
            }

            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param lostFxAssetInfo
     * @return
     */
    public List<MmsLostFixedAsset> searchLostItemByParameterPrefix(MmsLostFixedAsset lostFxAssetInfo) {
        Query query = em.createNamedQuery("MmsLostFixedAsset.findByAllParameters");
        query.setParameter("lostItemNo", '%' + lostFxAssetInfo.getLostItemNo() + '%');

        try {
            ArrayList<MmsLostFixedAsset> lostItemList = new ArrayList(query.getResultList());
            return lostItemList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public MmsLostFixedAsset getSelectedRequest(Integer lostItemId) {
        Query query = em.createNamedQuery("MmsLostFixedAsset.findByLostItemId");
        query.setParameter("lostItemId", lostItemId);
        System.err.println("===" + query.getResultList().size());
        try {
            MmsLostFixedAsset selectrequest = (MmsLostFixedAsset) query.getResultList().get(0);
            return selectrequest;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public MmsLostFixedAsset searchById(MmsLostFixedAsset LostObj) {

        try {
            System.out.println("inside facade");
            Query query = em.createNamedQuery("MmsLostFixedAsset.findByLostItemId");
            query.setParameter("lostItemId", LostObj.getLostItemId());

            return (MmsLostFixedAsset) query.getSingleResult();
        } catch (Exception ex) {
            
            return null;
        }
        
    }

    public List<MmsLostFixedAsset> searchLostItemByParameterPrefixAndLostPrep(MmsLostFixedAsset lostFxAssetEntity) {

        Query query = em.createNamedQuery("MmsLostFixedAsset.findByAllParametersAndEmpName");
        query.setParameter("lostItemNo", lostFxAssetEntity.getLostItemNo());
        query.setParameter("employeeName", lostFxAssetEntity.getEmployeeName());

        try {
            ArrayList<MmsLostFixedAsset> lostItemList = new ArrayList(query.getResultList());
            return lostItemList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<MmsLostFixedAsset> findLostListByWfStatus(int CHECK_APPROVE_VALUE) {
        Query query = em.createNamedQuery("MmsLostFixedAsset.findLostListByWfStatus", MmsLostFixedAsset.class);
        query.setParameter("lostStatus", CHECK_APPROVE_VALUE);
        try {
            ArrayList<MmsLostFixedAsset> listoflost = new ArrayList(query.getResultList());
            return listoflost;
        } catch (Exception e) {
            return null;
        }
    }

    public List<MmsLostFixedAsset> findLostListForCheckerByWfStatus(int PREPARE_VALUE, int APPROVE_REJECT_VALUE) {
        Query query = em.createNamedQuery("MmsLostFixedAsset.findLostListForCheckerByWfStatus", MmsLostFixedAsset.class);
        query.setParameter("prepared", PREPARE_VALUE);
        query.setParameter("approverReject", APPROVE_REJECT_VALUE);

        try {
            ArrayList<MmsLostFixedAsset> listofLost = new ArrayList(query.getResultList());
            return listofLost;
        } catch (Exception e) {
            return null;
        }
    }

    public List<MmsLostFixedAsset> searchAllLostInfoByPreparerId(Integer employeeName) {
        Query query = em.createNamedQuery("MmsLostFixedAsset.findAllByPreparerId", MmsLostFixedAsset.class);
        query.setParameter("employeeName", employeeName);
        System.out.println("======@facade====" + employeeName);
        try {
            ArrayList<MmsLostFixedAsset> LocList = new ArrayList(query.getResultList());
            return LocList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
}
