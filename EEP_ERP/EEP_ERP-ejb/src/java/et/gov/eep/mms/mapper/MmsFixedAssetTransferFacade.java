
package et.gov.eep.mms.mapper;

import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.mms.entity.MmsFixedAssetTransfer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author w_station
 */
@Stateless
public class MmsFixedAssetTransferFacade extends AbstractFacade<MmsFixedAssetTransfer> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     *
     */
    public MmsFixedAssetTransferFacade() {
        super(MmsFixedAssetTransfer.class);
    }

    /**
     *
     * @param TransferEntity
     * @return
     */
    public ArrayList<MmsFixedAssetTransfer> searchByTransferNo(MmsFixedAssetTransfer TransferEntity) {
        Query query = em.createNamedQuery("MmsFixedAssetTransfer.findByTransferNoLike");
        query.setParameter("transferNo", TransferEntity.getTransferNo() + '%');
        try {
            ArrayList<MmsFixedAssetTransfer> listoftransferNo = new ArrayList(query.getResultList());
            return listoftransferNo;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     *
     * @param TransferEntity
     * @return
     */
    public List<MmsFixedAssetTransfer> searchTransferByParameterPrefix(MmsFixedAssetTransfer TransferEntity) {
        Query query = em.createNamedQuery("MmsFixedAssetTransfer.findByAllParameters");
        query.setParameter("transferNo", '%' + TransferEntity.getTransferNo() + '%');
        try {
            ArrayList<MmsFixedAssetTransfer> transferList = new ArrayList(query.getResultList());
            return transferList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @return
     */
    public MmsFixedAssetTransfer getLastTransferId() {
        Query query = em.createNamedQuery("MmsFixedAssetTransfer.findByTransferIdMaximum");
        MmsFixedAssetTransfer result = null;
        try {
            if (query.getResultList().size() > 0) {
                result = (MmsFixedAssetTransfer) query.getResultList().get(0);
            }

            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public MmsFixedAssetTransfer getSelectedRequest(BigDecimal transferId) {
        Query query = em.createNamedQuery("MmsFixedAssetTransfer.findByTransferId");
        query.setParameter("transferId", transferId);
        System.err.println("===" + query.getResultList().size());
        try {
            MmsFixedAssetTransfer selectrequest = (MmsFixedAssetTransfer) query.getResultList().get(0);
            return selectrequest;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<MmsFixedAssetTransfer> searchTransferByParameterPrefixAndTrPrep(MmsFixedAssetTransfer TransferEntity) {

        Query query = em.createNamedQuery("MmsFixedAssetTransfer.findByAllParametersAndTrPrep");
        query.setParameter("transferNo", TransferEntity.getTransferNo());
        query.setParameter("preparedBy", TransferEntity.getPreparedBy());
        try {
            ArrayList<MmsFixedAssetTransfer> transferList = new ArrayList(query.getResultList());
            return transferList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<MmsFixedAssetTransfer> findTrListByWfStatus(int CHECK_REJECT_VALUE) {
        Query query = em.createNamedQuery("MmsFixedAssetTransfer.findTrListByWfStatus", MmsFixedAssetTransfer.class);
        query.setParameter("trStatus", CHECK_REJECT_VALUE);
        try {
            ArrayList<MmsFixedAssetTransfer> listoftr = new ArrayList(query.getResultList());
            return listoftr;
        } catch (Exception e) {
            return null;
        }
    }

    public List<MmsFixedAssetTransfer> findTrListForCheckerByWfStatus(int PREPARE_VALUE, int APPROVE_REJECT_VALUE) {
        Query query = em.createNamedQuery("MmsFixedAssetTransfer.findTrListForCheckerByWfStatus", MmsFixedAssetTransfer.class);
        query.setParameter("prepared", PREPARE_VALUE);
        query.setParameter("approverReject", APPROVE_REJECT_VALUE);

        try {
            ArrayList<MmsFixedAssetTransfer> listoftr = new ArrayList(query.getResultList());
            return listoftr;
        } catch (Exception e) {
            return null;
        }
    }

    public List<MmsFixedAssetTransfer> searchAlltraInfoByPreparerId(Integer preparedBy) {
        Query query = em.createNamedQuery("MmsFixedAssetTransfer.findAllByPreparerId", MmsFixedAssetTransfer.class);

        query.setParameter("preparedBy", preparedBy);
        System.out.println("======@facade====" + preparedBy);
        try {
            ArrayList<MmsFixedAssetTransfer> LocList = new ArrayList(query.getResultList());
            return LocList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

}
