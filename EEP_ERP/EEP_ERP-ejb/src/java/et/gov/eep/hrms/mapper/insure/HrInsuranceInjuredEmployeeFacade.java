/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package et.gov.eep.hrms.mapper.insure;

import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.hrms.entity.employee.HrEmployees;
import et.gov.eep.hrms.entity.insurance.HrInsuranceInjuredEmployee;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author meles
 */
@Stateless
public class HrInsuranceInjuredEmployeeFacade extends AbstractFacade<HrInsuranceInjuredEmployee> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HrInsuranceInjuredEmployeeFacade() {
        super(HrInsuranceInjuredEmployee.class);
    }

    //<editor-fold defaultstate="collapsed" desc="Bussiness IMmplementatin">
    public List<HrInsuranceInjuredEmployee> findtype(HrInsuranceInjuredEmployee hrInsuranceInjuredEmployee) {
        Query query = em.createNamedQuery("HrInsuranceInjuredEmployee.findByType", HrInsuranceInjuredEmployee.class);
        query.setParameter("type", hrInsuranceInjuredEmployee.getType().toUpperCase());
        try {
            
            return (List<HrInsuranceInjuredEmployee>) query.getResultList();
        } catch (Exception ex) {
            return null;
        }
    }
    
    public List<HrInsuranceInjuredEmployee> findakkll(HrInsuranceInjuredEmployee hrInsuranceInjuredEmployee) {
        Query query = em.createNamedQuery("HrInsuranceInjuredEmployee.ttt", HrInsuranceInjuredEmployee.class);
        query.setParameter("ttt", hrInsuranceInjuredEmployee.getType());
        try {
            
            return (List<HrInsuranceInjuredEmployee>) query.getResultList();
        } catch (Exception ex) {
            return null;
        }
    }
    
    public HrInsuranceInjuredEmployee getSelectedRequest(int id) {
        Query query = em.createNamedQuery("HrInsuranceInjuredEmployee.findById");
        query.setParameter("id", id);
        try {
            HrInsuranceInjuredEmployee selectrequest = (HrInsuranceInjuredEmployee) query.getResultList().get(0);
            return selectrequest;
        } catch (Exception ex) {
            return null;
        }
    }
    
    public List<HrInsuranceInjuredEmployee> findByfullname(HrInsuranceInjuredEmployee hrInsuranceInjuredEmployee) {
        Query query = em.createNamedQuery("HrInsuranceInjuredEmployee.findByFullName");
        query.setParameter("fullName", hrInsuranceInjuredEmployee.getFullName().toUpperCase() + '%');
        try {
            ArrayList<HrInsuranceInjuredEmployee> request = new ArrayList<>(query.getResultList());
            if (query.getResultList().isEmpty()) {
            } else if (query.getResultList().size() > 0) {
                return request;
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public List<HrInsuranceInjuredEmployee> findByempname(HrInsuranceInjuredEmployee hrInsuranceInjuredEmployee) {
        Query query = em.createNamedQuery("HrInsuranceInjuredEmployee.empname");
        query.setParameter("empname", hrInsuranceInjuredEmployee.getEmpId().getFirstName().toUpperCase() + '%');
        try {
            ArrayList<HrInsuranceInjuredEmployee> request = new ArrayList<>(query.getResultList());
            if (query.getResultList().isEmpty()) {
            } else if (query.getResultList().size() > 0) {
                return request;
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public List<HrInsuranceInjuredEmployee> findAll(HrInsuranceInjuredEmployee hrInsuranceInjuredEmployee) {
        Query query = em.createNamedQuery("HrInsuranceInjuredEmployee.findAll");
        try {
            ArrayList<HrInsuranceInjuredEmployee> request = new ArrayList<>(query.getResultList());
            if (query.getResultList().isEmpty()) {
            } else if (query.getResultList().size() > 0) {
                return request;
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public List<HrInsuranceInjuredEmployee> findByempname(HrEmployees hrEmployees) {
        Query query = em.createNamedQuery("HrInsuranceInjuredEmployee.empname");
        query.setParameter("empname", hrEmployees.getFirstName().toUpperCase() + '%');
        try {
            ArrayList<HrInsuranceInjuredEmployee> request = new ArrayList<>(query.getResultList());
            if (query.getResultList().isEmpty()) {
            } else if (query.getResultList().size() > 0) {
                return request;
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
//</editor-fold>
    
}
