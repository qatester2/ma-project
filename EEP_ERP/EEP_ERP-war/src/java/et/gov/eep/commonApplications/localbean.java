/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package et.gov.eep.commonApplications;

import java.io.Serializable;
import java.util.Locale;
import javax.inject.Named;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author meles
 */
@SessionScoped
@Named(value = "localbean")

public class localbean implements Serializable{
  private Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();

   
    /**
     * Creates a new instance of HeaderController
     */

//    private static final long serialVersionUID = 2756934361134603857L;
//    private static final Logger LOG = Logger.getLogger(HeaderController.class.getName());
    //private Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();

//    @PostConstruct
//    public void init() {
//        locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
//    }
    public Locale getLocale() {
        return locale;
    }

    public String getLanguage() {
        return locale.getLanguage();
    }

    public void changeLanguage(String language) {
        if (language.equalsIgnoreCase("ET")) {
            locale = new Locale("am", language);
            FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
        } else {
            locale = new Locale("en", language);
            FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
        }

    }

    public String englishAction() {
        FacesContext context = FacesContext.getCurrentInstance();
        locale = new Locale("am", "ET");
        context.getViewRoot().setLocale(locale);

        return null;
    }

    public String amAction() {
        FacesContext context = FacesContext.getCurrentInstance();
        locale = new Locale("am", "en");
        context.getViewRoot().setLocale(locale);

        return null;

    }

        
    public localbean() {
    }
    
}
