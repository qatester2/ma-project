package et.gov.eep.fcms.mapper.Ifrs;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.fcms.entity.Ifrs.FinancialInstrumentRegister;
import et.gov.eep.fcms.entity.admin.FmsSubsidiaryLedger;

/**
 *
 * @author mz
 */
@Stateless
public class FinancialInstrumentRegisterFacade extends AbstractFacade<FinancialInstrumentRegister> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FinancialInstrumentRegisterFacade() {
        super(FinancialInstrumentRegister.class);
    }
/*named query to select FmsSubsidiaryLedger value from FmsSubsidiaryLedger table using subsidiaryCode */
    public List<FmsSubsidiaryLedger> findbysubLedger(String subsidiaryLedger) {
        Query query = em.createNamedQuery("FmsSubsidiaryLedger.findBySubsidiaryCode");
        query.setParameter("subsidiaryCode", subsidiaryLedger);
        List<FmsSubsidiaryLedger> subsidiaryLedgerList = new ArrayList<>();
        if (query.getResultList().size() > 0) {
            subsidiaryLedgerList = query.getResultList();

        }
        return subsidiaryLedgerList;

    }
}
