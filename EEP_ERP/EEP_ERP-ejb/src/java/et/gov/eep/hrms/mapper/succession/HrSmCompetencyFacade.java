/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package et.gov.eep.hrms.mapper.succession;

import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.hrms.entity.succession.HrSmCompetency;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author meles
 */
@Stateless
public class HrSmCompetencyFacade extends AbstractFacade<HrSmCompetency> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HrSmCompetencyFacade() {
        super(HrSmCompetency.class);
    }

    //<editor-fold defaultstate="collapsed" desc="Named query"> 
    public List<HrSmCompetency> findbycompetencyname(HrSmCompetency hrSmCompetency) {
        Query query = em.createNamedQuery("HrSmCompetency.findByCompetencyNameLike");
        query.setParameter("competencyName", hrSmCompetency.getCompetencyName().toUpperCase() + '%');
        try {
            List<HrSmCompetency> compList = new ArrayList(query.getResultList());
            return compList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public boolean searchduplicate(HrSmCompetency hrSmCompetency) {
        boolean duplicaton;
        Query query = em.createNamedQuery("HrSmCompetency.searchbyduplicate", HrSmCompetency.class);
        query.setParameter("competencyName", hrSmCompetency.getCompetencyName());
        try {
            if (query.getResultList().size() > 0) {
                duplicaton = true;
            } else {
                duplicaton = false;
            }
            return duplicaton;
        } catch (Exception ex) {
            return false;
        }
    }

    public List<HrSmCompetency> findAll(HrSmCompetency hrSmCompetency) {
        Query query = em.createNamedQuery("HrSmCompetency.findAll");
        try {
            ArrayList<HrSmCompetency> request = new ArrayList<>(query.getResultList());
            if (query.getResultList().isEmpty()) {
            } else if (query.getResultList().size() > 0) {
                return request;
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    //</editor-fold>
}
