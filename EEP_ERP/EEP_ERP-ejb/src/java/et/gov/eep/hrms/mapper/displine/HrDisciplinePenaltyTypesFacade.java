/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package et.gov.eep.hrms.mapper.displine;

import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.hrms.entity.displine.HrDisciplinePenaltyTypes;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author user
 */
@Stateless
public class HrDisciplinePenaltyTypesFacade extends AbstractFacade<HrDisciplinePenaltyTypes> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HrDisciplinePenaltyTypesFacade() {
        super(HrDisciplinePenaltyTypes.class);
    }

    // <editor-fold defaultstate="collapsed" desc="Named query">

    public HrDisciplinePenaltyTypes findByPenalityCode(String toString) {
        HrDisciplinePenaltyTypes penaltyTypesObj;
        try {
            Query query = em.createNamedQuery("HrDisciplinePenaltyTypes.findById", HrDisciplinePenaltyTypes.class);
            query.setParameter("id", Integer.valueOf(toString));
            penaltyTypesObj = (HrDisciplinePenaltyTypes) query.getSingleResult();
            return penaltyTypesObj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<HrDisciplinePenaltyTypes> findByPenalityCode(HrDisciplinePenaltyTypes disciplinePenaltyTypes) {
        List<HrDisciplinePenaltyTypes> disciplinePenaltyTypeses = null;
        try {
            Query query = em.createNamedQuery("HrDisciplinePenaltyTypes.findByPenaltyCodesLike", HrDisciplinePenaltyTypes.class);
            query.setParameter("penaltyCode", disciplinePenaltyTypes.getPenaltyCode().toUpperCase() + '%');
            disciplinePenaltyTypeses = (List<HrDisciplinePenaltyTypes>) query.getResultList();
            return disciplinePenaltyTypeses;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<HrDisciplinePenaltyTypes> findByPenalityName(HrDisciplinePenaltyTypes disciplinePenaltyTypes) {
        List<HrDisciplinePenaltyTypes> disciplinePenaltyTypeses = null;
        try {
            Query query = em.createNamedQuery("HrDisciplinePenaltyTypes.findByPenaltyNameLike", HrDisciplinePenaltyTypes.class);
            query.setParameter("penaltyName", disciplinePenaltyTypes.getPenaltyName().toUpperCase() + '%');
            disciplinePenaltyTypeses = (List<HrDisciplinePenaltyTypes>) query.getResultList();
            return disciplinePenaltyTypeses;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<HrDisciplinePenaltyTypes> getPenalityListByName(String penalityName) {
        List<HrDisciplinePenaltyTypes> disciplinePenalityTypeses = null;
        try {
            Query query = em.createNamedQuery("HrDisciplinePenaltyTypes.findByPenaltyName", HrDisciplinePenaltyTypes.class);
            query.setParameter("penaltyName", penalityName);
            disciplinePenalityTypeses = (List<HrDisciplinePenaltyTypes>) query.getResultList();
            return disciplinePenalityTypeses;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean checkDuplicationByPenaltyName(HrDisciplinePenaltyTypes disciplinePenaltyTypes) {
        boolean duplicaton;
        Query query = em.createNamedQuery("HrDisciplinePenaltyTypes.findDuplicationByPenaltyName", HrDisciplinePenaltyTypes.class);
        query.setParameter("penaltyName", disciplinePenaltyTypes.getPenaltyName());
        try {
            if (query.getResultList().size() > 0) {
                duplicaton = true;
            } else {
                duplicaton = false;
            }
            return duplicaton;
        } catch (Exception ex) {
            return false;
        }
    }
    //</editor-fold>
}
