
package et.gov.eep.mms.mapper;

import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.mms.entity.MmsStockDisposal;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author w_station
 */
@Stateless
public class MmsStockDisposalFacade extends AbstractFacade<MmsStockDisposal> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MmsStockDisposalFacade() {
        super(MmsStockDisposal.class);
    }

    public List<MmsStockDisposal> searchStockDisposalByParameterPrefix(MmsStockDisposal stockDispEntity) {
        Query query = em.createNamedQuery("MmsStockDisposal.findByAllParameters");
        query.setParameter("stockNo", '%' + stockDispEntity.getStockNo() + '%');
        try {
            ArrayList<MmsStockDisposal> DisposalList = new ArrayList(query.getResultList());
            return DisposalList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public MmsStockDisposal getLastStockDisposalId() {

        Query query1 = em.createNamedQuery("MmsStockDisposal.findByStockIdMaximum");

        MmsStockDisposal result = null;

        try {
            if (query1.getResultList().size() > 0) {
                result = (MmsStockDisposal) query1.getResultList().get(0);
            } else {
                return result;
            }

            return result;
        } catch (Exception ex) {

            ex.printStackTrace();
            return null;
        }
    }

    public ArrayList<MmsStockDisposal> searchByStockDispNo(MmsStockDisposal stockDispEntity) {
        Query query = em.createNamedQuery("MmsStockDisposal.findByStockNoLike");
        query.setParameter("stockNo", stockDispEntity.getStockNo() + '%');
        try {
            ArrayList<MmsStockDisposal> listofStockDispNo = new ArrayList(query.getResultList());
            return listofStockDispNo;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<MmsStockDisposal> searchByStoreName1(MmsStockDisposal stockDispEntity) {
        Query query = em.createNamedQuery("MmsStockDisposal.findByAllParameters2");
        query.setParameter("storeId", '%' + stockDispEntity.getStoreId().getStoreName() + '%');
        try {
            ArrayList<MmsStockDisposal> DisposalList = new ArrayList(query.getResultList());
            return DisposalList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public MmsStockDisposal getSelectedRequest(Integer stockId) {
        Query query = em.createNamedQuery("MmsStockDisposal.findByStockId");
        query.setParameter("stockId", stockId);
        System.err.println("===" + query.getResultList().size());
        try {
            MmsStockDisposal selectrequest = (MmsStockDisposal) query.getResultList().get(0);
            return selectrequest;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<MmsStockDisposal> searchStockDisposalByParameterPrefixAndDispPrep(MmsStockDisposal stockDispEntity) {
        Query query = em.createNamedQuery("MmsStockDisposal.findByAllParametersAndDispPrep");
         query.setParameter("stockNo", '%' + stockDispEntity.getStockNo() + '%');
        try {
            ArrayList<MmsStockDisposal> DisposalList = new ArrayList(query.getResultList());
            return DisposalList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<MmsStockDisposal> findDispListByWfStatus(int PREPARE_VALUE) {
        Query query = em.createNamedQuery("MmsStockDisposal.findDispListByWfStatus", MmsStockDisposal.class);
        query.setParameter("stkStatus", PREPARE_VALUE);
        try {
            ArrayList<MmsStockDisposal> listofstk = new ArrayList(query.getResultList());
            return listofstk;
        } catch (Exception e) {
            return null;
        }
    }
 public List<MmsStockDisposal> findDispListByWfStatus1(int stkStatus) {
        Query query = em.createNamedQuery("MmsStockDisposal.findDispListByWfStatus1", MmsStockDisposal.class);
        query.setParameter("stkStatus", stkStatus);
        try {
            ArrayList<MmsStockDisposal> listofstk = new ArrayList(query.getResultList());
            return listofstk;
        } catch (Exception e) {
            return null;
        }
    }
  public List<MmsStockDisposal> searchAllDispInfoByPreparerId(Integer preparedBy) {
        Query query = em.createNamedQuery("MmsStockDisposal.findAllByPreparerId", MmsStockDisposal.class);

        query.setParameter("preparedBy", preparedBy);
        System.out.println("======@facade====" + preparedBy);
        try {
            ArrayList<MmsStockDisposal> LocList = new ArrayList(query.getResultList());
            return LocList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
