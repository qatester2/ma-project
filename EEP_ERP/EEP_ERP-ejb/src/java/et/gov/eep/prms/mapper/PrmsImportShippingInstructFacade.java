/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package et.gov.eep.prms.mapper;

import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.hrms.entity.organization.HrDepartments;
import et.gov.eep.prms.entity.PrmsImportShippingInstruct;
import et.gov.eep.prms.entity.PrmsLcRigistration;
import et.gov.eep.prms.entity.PrmsLcRigistrationAmend;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class PrmsImportShippingInstructFacade extends AbstractFacade<PrmsImportShippingInstruct> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PrmsImportShippingInstructFacade() {
        super(PrmsImportShippingInstruct.class);
    }

    public List<PrmsImportShippingInstruct> getgetISINo(PrmsImportShippingInstruct prmsImportShippingInstruct) {
        Query query = em.createNamedQuery("PrmsImportShippingInstruct.findByIsiNoLike");
        query.setParameter("isiNo", prmsImportShippingInstruct.getIsiNo() + '%');
        query.setParameter("preparedBy", prmsImportShippingInstruct.getPreparedBy());

        try {
            ArrayList<PrmsImportShippingInstruct> importShippingInstructs = new ArrayList<>(query.getResultList());
            return importShippingInstructs;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public PrmsImportShippingInstruct getSelectedId(String id) {
        Query query = em.createNamedQuery("PrmsImportShippingInstruct.findById");
        query.setParameter("id", id);
        try {
            PrmsImportShippingInstruct idlst = (PrmsImportShippingInstruct) query.getResultList().get(0);
            return idlst;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public PrmsImportShippingInstruct getLastISINo() {
        Query query = em.createNamedQuery("PrmsImportShippingInstruct.findByMaxId");
        PrmsImportShippingInstruct result = null;
        try {
            if (query.getResultList().size() > 0) {
                result = (PrmsImportShippingInstruct) query.getResultList().get(0);
            }

            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public HrDepartments getHrNames(int key) {

        Query query = em.createNamedQuery("HrDepartments.findByDepId");
        query.setParameter("depId", key);
        try {
            HrDepartments selectdepartment = (HrDepartments) query.getResultList().get(0);
            return selectdepartment;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<PrmsImportShippingInstruct> getImportshipsRequested() {
        Query query = em.createNamedQuery("PrmsImportShippingInstruct.findByReqForApprval");
        ArrayList<PrmsImportShippingInstruct> importshipsRequested = new ArrayList<>(query.getResultList());
        return importshipsRequested;
    }

    public List<PrmsLcRigistration> findApprovedLc(int approvedLc) {
        System.out.println("Status=======   " + approvedLc);
        Query q = em.createNamedQuery("PrmsLcRigistration.findByApprovedStatus");
        q.setParameter("status", approvedLc);
        try {
            List<PrmsLcRigistration> approvedLcLists = new ArrayList<>();
            if (q.getResultList().size() > 0) {
                approvedLcLists = q.getResultList();
            }
            return approvedLcLists;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public List<PrmsLcRigistrationAmend> checkingAsLcIsAmendedByLcId(PrmsLcRigistration prmsLcRigistration) {
        System.out.println("here checking by " + prmsLcRigistration.getLcId());
        Query q = em.createNativeQuery("SELECT * FROM prms_lc_rigistration_amend lcamd\n"
                + "inner join (SELECT lc_id ,max(id) as lcAmendMaxId  \n"
                + "FROM prms_lc_rigistration_amend\n"
                + "GROUP BY lc_id)lcamd2\n"
                + "on  lcamd.id=lcamd2.lcAmendMaxId\n"
                + "inner join prms_lc_rigistration lc\n"
                + "on lc.lc_id=lcamd.lc_id\n"
                + "where lc.lc_id='" + prmsLcRigistration.getLcId() + "'", PrmsLcRigistrationAmend.class);
        try {
            List<PrmsLcRigistrationAmend> lcNoAmendedList = new ArrayList<>();
            if (q.getResultList().size() > 0) {
                lcNoAmendedList = q.getResultList();
                System.out.println("Number of Amended with" + prmsLcRigistration.getLcId() + " Lc Id is " + lcNoAmendedList.size());
            }
            return lcNoAmendedList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PrmsLcRigistrationAmend getLcAmendedInfoByLcId(PrmsLcRigistration prmsLcRigistration) {
        System.out.println("get info by " + prmsLcRigistration.getLcId());
        Query query = em.createNativeQuery("SELECT * FROM prms_lc_rigistration_amend lcamd\n"
                + "inner join(SELECT max(id) as lcAmendedMaxId\n"
                + "FROM prms_lc_rigistration_amend )lcamd2\n"
                + "on lcamd.id=lcamd2.lcAmendedMaxId\n"
                + "where lcamd.lc_id='" + prmsLcRigistration.getLcId() + "'", PrmsLcRigistrationAmend.class);
        try {
            PrmsLcRigistrationAmend amendedLcInfo = new PrmsLcRigistrationAmend();
            if (query.getResultList().size() > 0) {
                amendedLcInfo = (PrmsLcRigistrationAmend) (query.getResultList().get(0));
                System.out.println("Lc Amount from Amended " + amendedLcInfo.getLcAmount());
            }
            return amendedLcInfo;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
