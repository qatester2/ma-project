package et.gov.eep.fcms.mapper.perDiem;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.fcms.entity.perDiem.FmsGoodWillingPayment;
import et.gov.eep.fcms.entity.perDiem.FmsLuAdditionalAmount;

/**
 *
 * @author muller
 */
@Stateless
public class FmsLuAdditionalAmountFacade extends AbstractFacade<FmsLuAdditionalAmount> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FmsLuAdditionalAmountFacade() {
        super(FmsLuAdditionalAmount.class);
    }
    
 /*named query to select allowance info from FmsLuAdditionalAmount table by levelId 
     returen levelId info*/
    public FmsLuAdditionalAmount searchLevel(FmsLuAdditionalAmount additionalAmount) {
        Query query = em.createNamedQuery("FmsLuAdditionalAmount.findByLevelId", FmsLuAdditionalAmount.class);
        query.setParameter("levelId", additionalAmount.getLevelId());
        try {
            FmsLuAdditionalAmount Level = (FmsLuAdditionalAmount) query.getResultList().get(0);
            return Level;
        } catch (Exception ex) {
            return null;
        }
    }

     /*named query to select allowance info from FmsLuAdditionalAmount table by levelId 
     returen levelId info*/
    public FmsLuAdditionalAmount search1(FmsLuAdditionalAmount additionalAmount) {
        Query query = em.createNamedQuery("FmsLuAdditionalAmount.findByLevelId");
        query.setParameter("levelId", additionalAmount.getLevelId());

        try {
            FmsLuAdditionalAmount levelList = (FmsLuAdditionalAmount) query.getResultList().get(0);
            return levelList;

        } catch (Exception ex) {
            return null;
        }
    }

     /*named query to select level list info from FmsGoodWillingPayment table by levelId 
     returen levelId info*/
    public FmsGoodWillingPayment getAddtionalData(FmsGoodWillingPayment fmsGoodWillingPayment) {
        Query query = em.createNamedQuery("FmsGoodWillingPayment.findAll");
        try {
            FmsGoodWillingPayment levelList = (FmsGoodWillingPayment) query.getResultList().get(0);
            return levelList;
        } catch (Exception ex) {
            return null;
        }
    }

      /*named query to select id info from FmsLuAdditionalAmount table by Id 
     returen leveldata info*/
    public FmsLuAdditionalAmount getdata(FmsLuAdditionalAmount additionalAmount) {
        Query query = em.createNamedQuery("FmsLuAdditionalAmount.findById");
        query.setParameter("id", additionalAmount.getId());

        try {
            FmsLuAdditionalAmount levelData = (FmsLuAdditionalAmount) query.getResultList().get(0);
            return levelData;
        } catch (Exception ex) {
            return null;
        }
    }

    /*named query to select id info from FmsLuAdditionalAmount table by Id 
     returen country info*/
    public FmsGoodWillingPayment getByGWId(FmsGoodWillingPayment fmsGoodWillingPayment) {
        Query query = em.createNamedQuery("FmsGoodWillingPayment.findById");
        query.setParameter("id", fmsGoodWillingPayment.getId());
        try {
            FmsGoodWillingPayment selectCounty = (FmsGoodWillingPayment) query.getResultList().get(0);
            return selectCounty;
        } catch (Exception ex) {
            return null;
        }
    }
  /*named query to select all additional amount list from FmsLuAdditionalAmount table 
     returen level list*/
    public List<FmsLuAdditionalAmount> listOfAdd(FmsLuAdditionalAmount additionalAmount) {
        List<FmsLuAdditionalAmount> levelLis;
        Query query = em.createNamedQuery("FmsLuAdditionalAmount.findAll");
        try {
            levelLis = (List<FmsLuAdditionalAmount>) query.getResultList();
            return levelLis;

        } catch (Exception ex) {
            return null;
        }
    }

      /*named query to select all additional amount level list from FmsLuAdditionalAmount table by levelID's first letter or name match 
     returen level list*/
    public List<FmsLuAdditionalAmount> searchLevelByParameter(FmsLuAdditionalAmount additionalAmount) {
        Query query = em.createNamedQuery("FmsLuAdditionalAmount.findByAllParameters");
        query.setParameter("levelId", additionalAmount.getLevelId().toUpperCase() + '%');
        try {
            ArrayList<FmsLuAdditionalAmount> levelList = new ArrayList(query.getResultList());
            return levelList;
        } catch (Exception ex) {
            return null;
        }
    }
}
