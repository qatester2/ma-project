/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package et.gov.eep.prms.mapper;

import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.fcms.entity.FmsLuCurrency;
import et.gov.eep.hrms.entity.address.HrAddresses;
import et.gov.eep.mms.entity.MmsItemRegistration;
import et.gov.eep.prms.entity.PrmsForeignExchange;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
@Stateless
public class PrmsForeignExchangeFacade extends AbstractFacade<PrmsForeignExchange> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PrmsForeignExchangeFacade() {
        super(PrmsForeignExchange.class);
    }

    //<editor-fold defaultstate="collapsed" desc="Named(Static) Queries">
    public PrmsForeignExchange findByfenumberObj(PrmsForeignExchange prmsForeignExchange) {
        Query query = em.createNamedQuery("PrmsForeignExchange.findByFeNumber");
        query.setParameter("feNumber", prmsForeignExchange.getFeNumber());
        try {
            PrmsForeignExchange fea = (PrmsForeignExchange) query.getResultList().get(0);
            return fea;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public ArrayList<PrmsForeignExchange> findByfenumber(PrmsForeignExchange prmsForeignExchange) {
        Query query = em.createNamedQuery("PrmsForeignExchange.findByFeNumberLike");
        query.setParameter("feNumber", prmsForeignExchange.getFeNumber() + "%");
        query.setParameter("preparedBy", prmsForeignExchange.getPreparedBy());

        try {
            ArrayList<PrmsForeignExchange> feaList = new ArrayList(query.getResultList());
            return feaList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

//generating next FE Number method
    public PrmsForeignExchange generateNextForeignExNo() {
        Query query = em.createNamedQuery("PrmsForeignExchange.InsertNextFE_No");
        PrmsForeignExchange exchange = null;
        try {
            if (query.getResultList().size() > 0) {
                exchange = (PrmsForeignExchange) query.getResultList().get(0);
            }
            return exchange;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    //get Foreign Exchange Request Lists available For Approval
    public List<PrmsForeignExchange> getForeignExchReqlist() {
        Query query = em.createNamedQuery("PrmsForeignExchange.findByForeignExchReqForApproval", PrmsForeignExchange.class);
        ArrayList<PrmsForeignExchange> foreignExchlist = new ArrayList<>(query.getResultList());
        return foreignExchlist;
    }
    //</editor-fold>
}
