
package et.gov.eep.mms.mapper;

import et.gov.eep.commonApplications.mapper.AbstractFacade;
import et.gov.eep.mms.entity.MmsInsurance;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author w_station
 */
@Stateless
public class MmsInsuranceFacade extends AbstractFacade<MmsInsurance> {

    @PersistenceContext(unitName = "EEP_ERP-ejbPU")
    private EntityManager em;

    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     *
     */
    public MmsInsuranceFacade() {
        super(MmsInsurance.class);
    }

    /**
     *
     * @param insuranceEntity
     * @return
     */
    public ArrayList<MmsInsurance> searchByInsuranceNo(MmsInsurance insuranceEntity) {
        Query query = em.createNamedQuery("MmsInsurance.findByInsNoLike");
        query.setParameter("insNo", insuranceEntity.getInsNo() + '%');
        try {
            ArrayList<MmsInsurance> listofInsuranceNo = new ArrayList(query.getResultList());
            return listofInsuranceNo;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     *
     * @param insuranceEntity
     * @return
     */
    public List<MmsInsurance> searchInsuranceByParameterPrefix(MmsInsurance insuranceEntity) {
        Query query = em.createNamedQuery("MmsInsurance.findByAllParameters");
        query.setParameter("insNo", '%' + insuranceEntity.getInsNo() + '%');
        try {
            ArrayList<MmsInsurance> insuranceList = new ArrayList(query.getResultList());
            return insuranceList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @return
     */
    public MmsInsurance getLastInsuranceId() {
        Query query1 = em.createNamedQuery("MmsInsurance.findByInsIdMaximum");

        MmsInsurance result = null;

        try {
            if (query1.getResultList().size() > 0) {
                result = (MmsInsurance) query1.getResultList().get(0);
            } else {
                return result;
            }

            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public MmsInsurance getSelectedRequest(Integer insId) {
        Query query = em.createNamedQuery("MmsInsurance.findByInsId");
        query.setParameter("insId", insId);
        System.err.println("===" + query.getResultList().size());
        try {
            MmsInsurance selectrequest = (MmsInsurance) query.getResultList().get(0);
            return selectrequest;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<MmsInsurance> findInsListByWfStatus(int insStatus) {
        Query query = em.createNamedQuery("MmsInsurance.findInsListByWfStatus", MmsInsurance.class);
        query.setParameter("insStatus", insStatus);
        try {
            ArrayList<MmsInsurance> listofins = new ArrayList(query.getResultList());
            return listofins;
        } catch (Exception e) {
            return null;
        }
    }

    public List<MmsInsurance> searchAllTransmissionsInfoByPreparerId(Integer authorizedBy) {
        Query query = em.createNamedQuery("MmsInsurance.findAllByPreparerId", MmsInsurance.class);

        query.setParameter("authorizedBy", authorizedBy);
        System.out.println("======@facade====" + authorizedBy);
        try {
            ArrayList<MmsInsurance> LocList = new ArrayList(query.getResultList());
            return LocList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
